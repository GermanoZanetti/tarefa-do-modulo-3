import java.util.Scanner;

public class Menu2 {

    public static void main(String[] args) {
        System.out.println("|   Menu        |");
        System.out.println("|   Opções:     |");
        System.out.println("|   1. Opção 1  |");
        System.out.println("|   2. Opção 2  |");
        System.out.println("|   3. Sair     |");

        Scanner menu = new Scanner(System.in);

        System.out.println("Digite a opção desejada: ");

        boolean sair = false;

        while(sair == false){

                    try{
                        int opcao = menu.nextInt();
                        switch (opcao) {
                            case 1:
                                System.out.println("Você escolheu a primeira opção");
                                break;
                            case 2:
                                System.out.println("Você escolheu a segunda opção");
                                break;
                            case 3:
                                System.out.println("O programa foi encerrado");
                                sair = true;
                                break;
                            default:
                                System.out.println("Seleção inválida");
                                break;
                        }
                    }catch(Exception inputException) {
                        System.out.println("Digite apenas números!");
                        menu.nextLine();
                    }
                }
            }
    }
