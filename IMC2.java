import java.text.DecimalFormat;
import java.util.Scanner;

public class IMC2 {

    public static void main(String[] args) {

        double peso;
        double altura;

        Scanner teclado = new Scanner (System.in);

        while(true) {

            double imc = 0;
            try {

                System.out.println("Digite o seu peso");
                peso = teclado.nextDouble();

                System.out.println("Informe a sua altura");
                altura = teclado.nextDouble();

                imc = peso / (altura * altura);

                if (imc < 18.5) {
                    System.out.println("Abaixo do peso ideal");
                } else if (imc >= 18.6 && imc <= 24.9) {
                    System.out.println("Dentro do peso ideal");
                } else if (imc >= 25 && imc <= 29.9) {
                    System.out.println("Excesso de peso");
                } else if (imc >= 30 && imc <= 34.9) {
                    System.out.println("Obesidade Grau I");
                } else if (imc >= 35 && imc <= 39.9) {
                    System.out.println("Obesidade Grau II (severa)");
                } else {
                    System.out.println("Obesidade Grau III (mórbida)");
                }

                DecimalFormat imcdecimal = new DecimalFormat("0.##");

                System.out.println("Seu IMC é: " + imcdecimal.format(imc));

            } catch (Exception inputException) {
                System.out.println("Digite apenas números!");
                teclado.nextLine();
            }

        }

    }
}
